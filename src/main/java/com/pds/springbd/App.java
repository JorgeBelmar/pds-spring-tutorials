package com.pds.springbd;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pds.beans.Equipo;
import com.pds.beans.Jugador;
import com.pds.beans.Marca;
import com.pds.service.ServiceJugador;
import com.pds.service.ServiceMarca;

public class App 
{
    public static void main( String[] args )
    {
    	//Tutorial 26: Ya no usaremos Marca (bean)
    	
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("com/pds/xml/beans.xml");
    	
    	//Tutorial 26: Se cambia = ServiceMarca sm = (ServiceMarca)appContext.getBean("serviceMarcaImpl");
    	ServiceJugador sm = (ServiceJugador)appContext.getBean("serviceJugadorImpl");

    	//Marca mar3 = (Marca)appContext.getBean("marca3"); //Tutorial 26: ahora en beans.xml
    	//Equipo eq1 = (Equipo)appContext.getBean("equipo1"); //Tutorial 26: ahora en beans.xml
    	Jugador jugador = (Jugador)appContext.getBean("jugador1");

    	try 
    	{
			//sm.registrar(mar3);
    		sm.registrar(jugador);
		} catch (Exception e) 
    	{
			System.out.println(e.getMessage());
		}
    }
}
