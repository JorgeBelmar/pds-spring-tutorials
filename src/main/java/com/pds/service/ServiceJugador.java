package com.pds.service;

import com.pds.beans.Jugador;

public interface ServiceJugador 
{
	public void registrar(Jugador jugador) throws Exception;
}
