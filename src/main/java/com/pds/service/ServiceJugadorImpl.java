package com.pds.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pds.beans.Jugador;
import com.pds.dao.DAOJugador;

@Service
public class ServiceJugadorImpl implements ServiceJugador
{
	@Autowired
	private DAOJugador daoJugador;
	
	public void registrar(Jugador jugador) throws Exception 
	{
		try 
		{
			daoJugador.registrar(jugador);
		} 
		catch (Exception e) 
		{
			throw e;
		}
	}

}
